[JPlanetSimulator](https://sites.google.com/site/jplanetsimulator/)
================

[JPlanet Simulator](https://sites.google.com/site/jplanetsimulator/) creates a 2D environment that anyone can place planets that will realistically orbit. In other words, it is a Planet Simulator

JPlanet simulator is hosted at [this](https://sites.google.com/site/jplanetsimulator/) website! The working jar file can be downloaded from there.

To compile, install gradle, and run `gradle build`. To run, simply run `gradle run`.

Hope you have some fun!
