# Use phusion/baseimage if problems arise

# This baseimage is based off of fghehm/docker-netbeans
FROM java:7
MAINTAINER Jay Kamat

# Setup apt to be happy with no console input
ENV DEBIAN_FRONTEND noninteractive


RUN apt-get update && apt-get install -y sudo && apt-get clean
# set up user (this is for running soccer later on)
# Replace 1000 with your user / group id
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer

USER developer
ENV HOME /home/developer

# do everything in root's home
WORKDIR /home/developer

# Add compiled files
COPY build/libs/Planet_Sim.jar /home/developer/

RUN ulimit -c unlimited

CMD java -jar /home/developer/Planet_Sim.jar
