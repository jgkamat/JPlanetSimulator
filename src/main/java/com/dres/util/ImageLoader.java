package com.dres.util;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import static com.dres.util.Out.*;

public class ImageLoader {
	public static Image failedLoad;
	static {
		//outLoggingStart();
		BufferedImage fl = new BufferedImage(800, 600, BufferedImage.TYPE_INT_RGB);
		failedLoad = fl;
		Graphics2D g2 = fl.createGraphics();
		g2.drawString("Error loading resource + s", 10, 20);
		g2.dispose();
	}

	public static Image load(String s) {
		return load(s, null);
	}
	public static Image load(String s, Class<?> c){
		// Image bkg;//Result
		try {
			//outln("called on " + s);
			File f = new File(s); // Try the easiest way.
			//outln(f.getAbsolutePath());
			Image i = ImageIO.read(f);
			waitForImage(i);
			//outln("Completed! " + i);
			return i;
		} catch (Exception e) {
			try { // Try the original way
				//printStackTrace(e);
				if (c == null)
					c = e.getClass();
				return (new javax.swing.ImageIcon(c.getResource(s)))
						.getImage();
			} catch (NullPointerException e2) {
				//outln("ST1");
				//printStackTrace(e2);
				// Try it again
				if (s.indexOf('/') == 0 || s.indexOf('\\') == 0) {
					/*out("True for " + s);
					if (s.indexOf('/') == 0)
						outln(" /");
					else if (s.indexOf('\\') == 0)
						outln(" \\");
					else
						outln(" ??");*/
					String s2 = s.substring(1);
					//outln("Recursive call on " + s2);
					return load(s2);
				}
				//outln("ST2");
				//printStackTrace(e);
				//errln("Failed to load resource + s");
				return waitForImage(failedLoad); // Damnit.
			}
        }
	}
	
	public static Image waitForImage(Image i){
		MediaTracker mt = new MediaTracker(new Component(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 7003728988970326289L;});
		mt.addImage(i, 0);
		try {
			mt.waitForAll();
		} catch (InterruptedException e) {
			outln("Waiting interrupted");
		}
		return i;
	}

}
