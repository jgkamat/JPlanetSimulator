package com.dres.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;

public class Out {
	
	public static boolean printArrayData = true;
	
	public static void out(boolean b){
		System.out.print(b);
		bufferWrite(Boolean.toString(b));
	}
	public static void out(char c){
		System.out.print(c);
		bufferWrite(Character.toString(c));
	}
	public static void out(char[] carr){
		String s;
		if (printArrayData) {
			s = arrayDataToString(carr);
		} else {
			s = carr.toString();
		}
		System.out.print(s);
		bufferWrite(s);
	}
	public static void out(int[] carr){
		String s;
		if (printArrayData) {
			s = arrayDataToString(carr);
		} else {
			s = carr.toString();
		}
		System.out.print(s);
		bufferWrite(s);
	}
	public static void out(double d){
		System.out.print(d);
		bufferWrite(Double.toString(d));
	}
	public static void out(float f){
		System.out.print(f);
		bufferWrite(Float.toString(f));
	}
	public static void out(long l){
		System.out.print(l);
		bufferWrite(Long.toString(l));
	}
	public static void out(int i){
		System.out.print(i);
		bufferWrite(Integer.toString(i));
	}
	public static void out(String s){
		System.out.print(s);
		bufferWrite(s);
	}
	public static void out(Object o){
		System.out.print(o);
		bufferWrite(o.toString());
	}
	public static void outln(){
		System.out.println();
		bufferWriteln("");
	}
	public static void outln(boolean b){
		System.out.println(b);
		bufferWriteln(Boolean.toString(b));
	}
	public static void outln(char c){
		System.out.println(c);
		bufferWriteln(Character.toString(c));
	}
	public static void outln(char[] carr){
		String s;
		if (printArrayData) {
			s = arrayDataToString(carr);
		} else {
			s = carr.toString();
		}
		System.out.println(s);
		bufferWriteln(s);
	}
	public static void outln(int[] carr){
		String s;
		if (printArrayData) {
			s = arrayDataToString(carr);
		} else {
			s = carr.toString();
		}
		System.out.println(s);
		bufferWriteln(s);
	}
	public static void outln(double d){
		System.out.println(d);
		bufferWriteln(Double.toString(d));
	}
	public static void outln(float f){
		System.out.println(f);
		bufferWriteln(Float.toString(f));
	}
	public static void outln(long l){
		System.out.println(l);
		bufferWriteln(Long.toString(l));
	}
	public static void outln(int i){
		System.out.println(i);
		bufferWriteln(Integer.toString(i));
	}
	public static void outln(String s){
		System.out.println(s);
		bufferWriteln(s);
	}
	public static void outln(Object o){
		System.out.println(o);
		bufferWriteln(o.toString());
	}
	
	public static void err(boolean b){
		System.err.print(b);
		eBufferWrite(Boolean.toString(b));
	}
	public static void err(char c){
		System.err.print(c);
		eBufferWrite(Character.toString(c));
	}
	public static void err(char[] carr){
		String s;
		if (printArrayData) {
			s = arrayDataToString(carr);
		} else {
			s = carr.toString();
		}
		System.err.print(s);
		eBufferWrite(s);
	}
	public static void err(int[] carr){
		String s;
		if (printArrayData) {
			s = arrayDataToString(carr);
		} else {
			s = carr.toString();
		}
		System.err.print(s);
		eBufferWrite(s);
	}
	public static void err(double d){
		System.err.print(d);
		eBufferWrite(Double.toString(d));
	}
	public static void err(float f){
		System.err.print(f);
		eBufferWrite(Float.toString(f));
	}
	public static void err(long l){
		System.err.print(l);
		eBufferWrite(Long.toString(l));
	}
	public static void err(int i){
		System.err.print(i);
		eBufferWrite(Integer.toString(i));
	}
	public static void err(String s){
		System.err.print(s);
		eBufferWrite(s);
	}
	//TODO: Fix
	public static void err(Object o){
		System.err.print(o);
		eBufferWrite(o.toString());
	}
	
	
	public static void errln(){
		System.err.println();
		eBufferWriteln("");
	}
	public static void errln(boolean b){
		System.err.println(b);
		eBufferWriteln(Boolean.toString(b));
	}
	public static void errln(char c){
		System.err.println(c);
		eBufferWriteln(Character.toString(c));
	}
	public static void errln(char[] carr){
		String s;
		if (printArrayData) {
			s = arrayDataToString(carr);
		} else {
			s = carr.toString();
		}
		System.err.print(s);
		eBufferWriteln(s);
	}
	public static void errln(int[] carr){
		String s;
		if (printArrayData) {
			s = arrayDataToString(carr);
		} else {
			s = carr.toString();
		}
		System.err.print(s);
		eBufferWriteln(s);
	}
	public static void errln(double d){
		System.err.println(d);
		eBufferWriteln(Double.toString(d));
	}
	public static void errln(float f){
		System.err.println(f);
		eBufferWriteln(Float.toString(f));
	}
	public static void errln(long l){
		System.err.println(l);
		eBufferWriteln(Long.toString(l));
	}
	public static void errln(int i){
		System.err.println(i);
		eBufferWriteln(Integer.toString(i));
	}
	public static void errln(String s){
		System.err.println(s);
		eBufferWriteln(s);
	}
	//TODO: Fix
	public static void errln(Object o){
		System.err.println(o);
		eBufferWriteln(o.toString());
	}
	
	public static void printStackTrace(Throwable t){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		errln(sw.toString());
	}
	
	public static void toLog(String s){
		bufferWrite(s);
	}
	
	public static void toErLog(String s){
		eBufferWrite(s);
	}

	private static String arrayDataToString(int[] in){
		if(in.length == 0)
			return "{ }";
		StringBuilder ca = new StringBuilder().append('{');
		for (int c : in)
			ca.append(c).append(", ");
		ca.setLength(ca.length()-2);
		ca.append('}');
		return ca.toString();
	}
	private static String arrayDataToString(char[] in){
		if(in.length == 0)
			return "{ }";
		StringBuilder ca = new StringBuilder().append('{');
		for (char c : in)
			ca.append(c).append(", ");
		ca.setLength(ca.length()-2);
		ca.append('}');
		return ca.toString();
	}
	
	
	public static void outLoggingStart(){
		outLoggingStart(new File(Long.toString(System.currentTimeMillis()).substring(3)));
	}
	private static File root;
	private static final int LIM = 250000;
	private static int err = 0, out = 0;
	@SuppressWarnings("unused")
	private static final int ERR = 1, OUT = 0;
	private static BufferedWriter ow, ew;
	public static void outLoggingStart(File f){
		root = f;
		f.mkdir();
		ew = makeOut("err");
		ow = makeOut("out");
	}
	private static BufferedWriter makeOut(String name){
		File out = new File(root, "0." + name);
		for(int i = 1; out.exists(); i++)
			out = new File(root, i + "." + name);
		try{
			return new BufferedWriter(new FileWriter(out));
		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	private static void bufferWriteln(String s){
		write(s, ow, true);
	}
	private static void eBufferWriteln(String s){
		write(s, ew, true);
	}
	
	private static void bufferWrite(String s){
		write(s, ow, false);
	}
	private static void eBufferWrite(String s){
		write(s, ew, false);
	}
	private static void write(String s, BufferedWriter bw, boolean newline){
		if(bw != null){
			synchronized(bw){
				if(s == null)
					s = "null";
				try{
					if(newline) s += "\n";
					bw.write(s);
					bw.flush();
					if(bw == ow){
						out += s.length();
					}else if(bw == ew){
						err += s.length();
					}
					if(err >= LIM || out >= LIM){
						try{
							bw.close();
						}catch(Exception e){
							e.printStackTrace();
							throw new RuntimeException(e);
						}
						if(bw == ew){
							err = 0;
							ew = makeOut("err");
						}else if(bw == ow){
							out = 0;
							ew = makeOut("out");
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
}
