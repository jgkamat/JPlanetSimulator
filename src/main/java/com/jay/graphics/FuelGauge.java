/*
 * **************************************************************************
 * Copyright © 2011-2014 Jay Kamat
 * 
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 * 
 * This file is part of JPlanet Simulator.
 * 
 * JPlanet Simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPlanet Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JPlanet Simulator.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package com.jay.graphics;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import com.jay.PlanetSim;


public class FuelGauge extends JPanel{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -5469597191239840861L;
	PlanetSim window;
    
    public FuelGauge(PlanetSim p){
        super();
        window=p;
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        if(window.getGameMode()==2){
            g.setColor(Color.BLACK);
            g.setColor(new Color((int)((DisplayPanel.getStartfuel()-window.panel2.fuel)*.255),(int)(window.panel2.fuel*.255),0));
            g.fillRect(0, 0, window.panel2.fuel/10, 20);
            g.setColor(Color.BLACK);
            g.drawRect(0, 0, DisplayPanel.getStartfuel()/10, 20);
        }
    }
}
