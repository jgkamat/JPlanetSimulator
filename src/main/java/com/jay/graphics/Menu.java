/*
 * **************************************************************************
 * Copyright © 2011-2014 Jay Kamat
 * 
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 * 
 * This file is part of JPlanet Simulator.
 * 
 * JPlanet Simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPlanet Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JPlanet Simulator.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package com.jay.graphics;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.jay.PlanetSim;

/*
 * Jay Kamat 
 * Period 5 
 * Version 2.3
 */
public class Menu extends JPanel implements ActionListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1781244694800153420L;
	private Image bkg;
    private TransparentButton GM1,GM2,GM3,Help,Credits,Exit;
    private int width,height;
    private PlanetSim window;
    public static final int buttonHeight=50,buttonWidth=300;
    private static final String credits="JPlanetSimulator Version: "+
    		PlanetSim.version+"\n\nMaking the Program: Jay Kamat\n"
            + "Image Manipulation: Gimp\n"
            + "Sound Manipulation: Audacity\n" +
            "Sound Playback: JLayer 1.1\n\n"
            + "Inspiration: Mr. Shelby and Ms. Fackrell"
            + "";
    
    private static final String instructions="In Creative Mode:\n\n"
            + "left click to place a planet with no speed\n"
            + "left click and drag to place a planet with speed\n"
            + "right click to place a star\n"
            + "mouse to the top to pause\n"
            + "double click fields to edit them\n"
            + "for masses, change is only on new planets/stars\n"
            + "Press Esc to delete everything (exept starter sun)\n"
            + "Check Edu mode to show the forces active on planets\n"
            + "Slide the slider in the upper right to slow things down\n\n"
            + "In Survival Mode:\n\n"
            + "Use left/right arrow keys to control spaceship\n"
            + "use up key to start thrusters\n"
            + "try not to crash into planets/stars that spawn!\n"
            + "watch out for your fuel! (shown in the top panel)\n"
            + "your score and highscore are displayed in the top panel\n\n"
            + "TicTacToe Mode:\n\n"
            + "If you become bored, try out the Tictactoe game";
    public Menu(PlanetSim windows) {
        super();

        window=windows;

        this.setLayout(null);
        repaint();
        bkg = (new javax.swing.ImageIcon(getClass().getResource
                ("/images/MenuBackground.jpg"))).getImage();
        GM1=new TransparentButton("Creative");
        GM2=new TransparentButton("Survival");
        GM3=new TransparentButton("TicTacToe");
        Credits=new TransparentButton("About");
        Exit=new TransparentButton("Quit");
        Help=new TransparentButton("Instructions");
        
        GM1.addActionListener(this);
        GM2.addActionListener(this);
        GM3.addActionListener(this);
        Help.addActionListener(this);
        Credits.addActionListener(this);
        Exit.addActionListener(this);

        //just in case, for the keylistener
        GM1.setFocusable(false);
        GM2.setFocusable(false);
        GM3.setFocusable(false);
        Help.setFocusable(false);
        Credits.setFocusable(false);
        Exit.setFocusable(false);
        this.setFocusable(false);
        
        this.add(GM1);
        this.add(GM2);
        this.add(GM3);
        this.add(Help);
        this.add(Credits);
        this.add(Exit);
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        //self explanitory? just make a menu with buttons...
        super.paintComponent(g);
        
        GM1.setBounds(new Rectangle(width/2-buttonWidth/2,height/4,buttonWidth,buttonHeight));
        GM2.setBounds(new Rectangle(width/2-buttonWidth/2,height/4+height/8,buttonWidth,buttonHeight));
        GM3.setBounds(new Rectangle(width/2-buttonWidth/2,height/4+height/4,buttonWidth,buttonHeight));
        Help.setBounds(new Rectangle(width/2-buttonWidth/2,height/4+height*3/8,buttonWidth,buttonHeight));
        Credits.setBounds(new Rectangle(width/2-buttonWidth/2,height/4+height/2,buttonWidth,buttonHeight));
        Exit.setBounds(new Rectangle(width/2-buttonWidth/2,height/4+height*5/8,buttonWidth,buttonHeight));
        
        width = getWidth();
        height = getHeight();
        
        
        g.drawImage(bkg, 0, 0, width, height, this);
    }

    public void actionPerformed(ActionEvent e) {
        JButton click = (JButton)e.getSource();
        
        //runs stuff when you hit a button
        if(click==GM1){
            window.runGM1();
        }
        else if(click==GM2){
            window.runGM2();
        }
        else if(click==GM3){
            PlanetSim.runGM3();
        }
        else if(click==Credits){
            JOptionPane.showMessageDialog(null, credits);
        }
        else if(click==Exit){
            System.exit(0);
        }
        else if(click==Help){
            JOptionPane.showMessageDialog(null, instructions);
        }
    }
}
