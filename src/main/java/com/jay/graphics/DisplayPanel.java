/*
 * **************************************************************************
 * Copyright © 2011-2014 Jay Kamat
 * 
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 * 
 * This file is part of JPlanet Simulator.
 * 
 * JPlanet Simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPlanet Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JPlanet Simulator.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package com.jay.graphics;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.jay.PlanetSim;
import com.jay.simulation.Planet;
import com.jay.simulation.Star;

/*
 * Jay Kamat
 * Version 3.0
 */
public class DisplayPanel extends JPanel implements ActionListener,ChangeListener {
    //displayPanel holds ALL THE CONSTANTS (and variables) such as mass, fuel, ect.
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -6116137521005237650L;
	private JTextArea planetAmount,starAmount,gPrompt,gType,pMassPrompt,pMassType,
            sMassPrompt,sMassType,fuelAmount,scoreAmount,hScoreAmount;
    private JPanel viewPane,fuelGauge;
    private JCheckBox startStar,planetGravity,eduMode;
    private JSlider fpsSlider;
    private JButton exit;
    private int starCount=0,planetCount=0;
    private PlanetSim window;
    private double G; //Edit defaults in resetDefaults();
    private double starMass;
    private double planetMass;
    private static final int startFuel=1000;
    public int fuel=getStartfuel();
    public int score=0;
    public int highScore=0;
    public int delay=30;
    
    public DisplayPanel(PlanetSim windows){
        super(new BorderLayout());

        window=windows;
        
        viewPane=new JPanel(new FlowLayout());
        viewPane.setBackground(Color.WHITE);
        
        fuelGauge=new FuelGauge(window);
        fuelGauge.setBounds(5, 5, 200, 50);
        fuelGauge.setBackground(Color.WHITE);
        
        startStar=new JCheckBox("StartStar",true);
        startStar.setBackground(Color.WHITE);
        
        planetGravity=new JCheckBox("PlanetGravity",true);
        planetGravity.setBackground(Color.WHITE);
        
        eduMode=new JCheckBox("Edu Mode",false);
        eduMode.setBackground(Color.WHITE);

        fpsSlider=new JSlider(JSlider.HORIZONTAL,5,55,30);
        fpsSlider.addChangeListener(this);
        fpsSlider.setMajorTickSpacing(10);
        fpsSlider.setPaintTicks(true);
        fpsSlider.setPaintLabels(true);

        resetDefaults();

        //all this complicated stuff is so the user has an easy way to edit everything...
        planetAmount=new JTextArea("Planets: 0",1,1);
        starAmount=new JTextArea("Stars: 0",1,1);
        gPrompt=new JTextArea("G:",1,1);
        gType=new JTextArea(Double.toString(G),1,1);
        pMassPrompt=new JTextArea("Planet Mass:",1,1);
        pMassType=new JTextArea(Double.toString(planetMass),1,1);
        sMassPrompt=new JTextArea("Star Mass:",1,1);
        sMassType=new JTextArea(Double.toString(starMass),1,1);
        fuelAmount=new JTextArea(null,1,1);
        scoreAmount=new JTextArea(null,1,1);
        hScoreAmount=new JTextArea("High Score"+highScore,1,1);
        
        exit=new JButton("Exit");
        exit.addActionListener(this);
        
        //sets the gamemode properly
        refreshGM();
        refreshFuel();
        refreshScore();
        
        //just in case...for the keylistener
        gPrompt.setFocusable(false);
        planetAmount.setFocusable(false);
        starAmount.setFocusable(false);
        pMassPrompt.setFocusable(false);
        sMassPrompt.setFocusable(false);
        startStar.setFocusable(false);
        exit.setFocusable(false);
        fuelAmount.setFocusable(false);
        scoreAmount.setFocusable(false);
        hScoreAmount.setFocusable(false);
        fpsSlider.setFocusable(false);
        viewPane.setFocusable(false);
        fuelGauge.setFocusable(false);
        planetGravity.setFocusable(false);
        eduMode.setFocusable(false);
        
        //hmmm...this is fun stuff
        viewPane.add(planetAmount);
        viewPane.add(starAmount);
        viewPane.add(gPrompt);
        viewPane.add(gType);
        viewPane.add(pMassPrompt);
        viewPane.add(pMassType);
        viewPane.add(sMassPrompt);
        viewPane.add(sMassType);
        viewPane.add(startStar);
        viewPane.add(planetGravity);
        viewPane.add(eduMode);
        viewPane.add(fuelAmount);
        viewPane.add(scoreAmount);
        viewPane.add(hScoreAmount);
        viewPane.add(exit);
        
        this.add(fuelGauge,BorderLayout.EAST);
        this.add(viewPane,BorderLayout.CENTER);
        
        
        this.add(fpsSlider,BorderLayout.LINE_END);
        this.setFocusable(false);

        fpsSlider.setBackground(Color.white);
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        fuelGauge.repaint();
    }
    
    public void refreshGM(){
        planetAmount.setEditable(false);
        starAmount.setEditable(false);
        gPrompt.setEditable(false);
        pMassPrompt.setEditable(false);
        sMassPrompt.setEditable(false);
        fuelAmount.setEditable(false);
        scoreAmount.setEditable(false);
        hScoreAmount.setEditable(false);
        if(window.getGameMode()==2){
            //default values...
            resetDefaults();
            //display the change
            refreshTypes();
            
            hScoreAmount.setText("High Score "+highScore);
            planetGravity.setSelected(false);

            gType.setVisible(false);
            pMassType.setVisible(false);
            sMassPrompt.setVisible(false);
            startStar.setVisible(false);
            sMassType.setVisible(false);
            gPrompt.setVisible(false);
            pMassPrompt.setVisible(false);
            sMassPrompt.setVisible(false);
            planetGravity.setVisible(false);
            eduMode.setVisible(false);
            fpsSlider.setVisible(false);
            
            fuelAmount.setVisible(true);
            scoreAmount.setVisible(true);
            hScoreAmount.setVisible(true);
        }
        else if(window.getGameMode()==1){
            planetGravity.setSelected(true);
            
            gType.setVisible(true);
            pMassType.setVisible(true);
            startStar.setVisible(true);
            sMassType.setVisible(true);
            gPrompt.setVisible(true);
            pMassPrompt.setVisible(true);
            sMassPrompt.setVisible(true);
            planetGravity.setVisible(true);
            eduMode.setVisible(true);
            fpsSlider.setVisible(true);
            
            fuelAmount.setVisible(false);
            scoreAmount.setVisible(false);
            hScoreAmount.setVisible(false);

            resetScore();
            resetFuel();
            fuelAmount.setVisible(false);
            scoreAmount.setVisible(false);
            hScoreAmount.setVisible(false);
        }
    }
    
    private void resetDefaults(){
        startStar.setSelected(true);
        planetGravity.setSelected(true);
        eduMode.setSelected(false);
        starMass=Star.defaultStarMass;
        planetMass=Planet.defaultPlanetMass;
        G=20;
        fpsSlider.setValue(30);
    }
    
    public void refreshTypes(){
        pMassType.setText(Double.toString(planetMass));
        sMassType.setText(Double.toString(starMass));
        gType.setText(Double.toString(G));
    }
    
    public void refreshScore(){
        scoreAmount.setText("Score: "+score);
        if(score>highScore){
            highScore=score;
            hScoreAmount.setText("High Score "+highScore);
        }
    }
    
    public void resetScore(){
        score=0;
        refreshScore();
    }
    
    public void refreshFuel(){
        repaint();
        fuelAmount.setText("Fuel: "+fuel);
    }
    
    public static int getStartfuel() {
		return startFuel;
	}

	public void resetFuel(){
        fuel=DisplayPanel.getStartfuel();
        refreshFuel();
    }
    
    public void clearCount(){
        //sets the amounts back to zero
        starCount=0; planetCount=0;
        planetAmount.setText("Planets: 0");
        starAmount.setText("Stars: 0");
    }
    
    public double getG(){
        //gets the G so everyone can see
        refresh();
        return G;
    }
    
    public double getPMass(){
        //gets the Planet mass so everyone can see
        refresh();
        return planetMass;
    }
    
    public double getSMass(){
        //gets the Star mass so everyone can...you get the idea
        refresh();
        return starMass;
    }
    
    public boolean getStartStar(){
        //got star?
        return startStar.isSelected();
    }
    
    public boolean getPlanetGravity(){
        return planetGravity.isSelected();
    }
    
    public boolean getEduMode(){
        return eduMode.isSelected();
    }
    
    public void refresh(){
        //gets input from the textboxes
        checkForErrors(gType);
        checkForErrors(pMassType);
        checkForErrors(sMassType);
        G=Double.parseDouble(gType.getText());
        planetMass=Double.parseDouble(pMassType.getText());
        starMass=Double.parseDouble(sMassType.getText());
    }
    
    public void checkForErrors(JTextArea in){
        boolean onePoint=false;
        if("".equals(in.getText())||in.getText()==null){
            resetDefaults(); //restores defaults (user=stupid, he gets punished :P()
            refreshTypes();
            //well at least it stops the program from crashing...
        }
        for(int counter=0;counter<in.getText().length();counter++){
            //if its not a number then kaboom
            if(in.getText()!=null&&(Character.isLetter(in.getText().charAt(counter))
                    ||!Character.isLetterOrDigit(in.getText().charAt(counter)))){
                if(in.getText().charAt(counter)!='.'){
                    resetDefaults();
                    refreshTypes();
                    break;
                }
                if(in.getText().charAt(counter)=='.'&&onePoint){
                    resetDefaults();
                    refreshTypes();
                    break;
                }
                if(in.getText().charAt(counter)=='.'){
                    onePoint=true;
                }
            }
        }
    }
    
    public void setStarCount(int amount){
        //adds a star to the total count
        starCount=amount;
        starAmount.setText("Stars: "+starCount);
    }
    
    public void setPlanetCount(int amount){
        //adds a planet to the total count
        planetCount=amount;
        planetAmount.setText("Planets: "+planetCount);
    }

    public void actionPerformed(ActionEvent e) {
        window.runMenu();
    }

    public void stateChanged(ChangeEvent e) {
        JSlider source = (JSlider)e.getSource();
        if (!source.getValueIsAdjusting()) {
            int fps = (int)source.getValue();
            delay=1000/fps;
        }
    }
}
