/*
 * **************************************************************************
 * Copyright © 2011-2014 Jay Kamat
 * 
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 * 
 * This file is part of JPlanet Simulator.
 * 
 * JPlanet Simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPlanet Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JPlanet Simulator.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package com.jay.graphics;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JButton;

/*
 * Some random internet source
 * <Not made by me(exept some simple edits)>
 * just a class to make buttons translucent
 */

public class TransparentButton extends JButton {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6085370686142509034L;

	public TransparentButton(String text) { 
        super(text);
        this.setFont(new Font(Font.SANS_SERIF,Font.PLAIN,20));
        setOpaque(false); 
        this.setForeground(Color.black);
    } 

    @Override
    public void paint(Graphics g) { 
        Graphics2D g2 = (Graphics2D) g.create(); 
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6f));  
        //modify transparency by changing the <double>f field over........here ^
        super.paint(g2); 
        g2.dispose(); 
    } 
}