/*
 ****MovingImage****
This class represents an Image that can easily be moved around the screen.

To use this class:
-Initalize your object using assumed coordinates.
-All instance methods that take in coordinates are taking in *assumed* 
 coordinates
-Before calling any other method, and anytime there is a possibility that 
 the window has changed size, call the setActualPanelSize() method.

Author: Shelby
 * Modded: Jay
Date: 5/1/12
 * ModDate:5/11/12
Version 2.1
 */

package com.jay.graphics;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.ImageObserver;

public class MovingImage {

	// FIELDS
	public static int DRAWING_WIDTH=0;
	public static int DRAWING_HEIGHT=0;
	//resizing is not needed, as a larger window is just more space to play with
	//The user places all objects, so the window looks good with all screens

	private Image img;
	private boolean isVisible;
	protected double x, y;
	private int width, height;

	// CONSTRUCTOR
	/*
	 All coordinates are in assumed coordinates and represent data for the
	 image, not the window.
	 */
	public MovingImage(Image image, double x, double y, int w, int h) {
		img = image;
		isVisible = true;
		this.x = x;
		this.y = y;
		width = w;
		height = h;
	}


	// NON-STATIC METHODS
	/*
	 * MovingImage "jumps" to the location passed in as arguments.
	 */
	public void moveTo(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public static void setSize(int width,int height){
		DRAWING_WIDTH=width;
		DRAWING_HEIGHT=height;
	}

	/*
	 * MovingImage moves left/right or up/down by the amount passed
	 * in as arguments. Pass a negative number to move the other direction.
	 */
	public void move(double x, double y) {
		this.x += x;
		this.y += y;
		if (this.x < 0) {
			this.x = 0;
		}
		if (this.y < 0) {
			this.y = 0;
		}

		if (this.x+width > DRAWING_WIDTH) {
			this.x = DRAWING_WIDTH-width;
		}
		if (this.y+height > DRAWING_HEIGHT) {
			this.y = DRAWING_HEIGHT-height;
		}
	}

	/*
	 * Returns the image's x coordinate.
	 */
	public double getX() {
		return x;
	}

	/*
	 * Returns the image's y coordinate.
	 */
	public double getY() {
		return y;
	}
	
	public double getXC(){
		return x-width/2;
	}
	
	public double getYC(){
		return y-height/2;
	}

	public Image getImage(){
		return img;
	}

	public void setImage(Image i){
		img=i;
	}

	public int getHeight(){
		return height;
	}

	public int getWidth(){
		return width;
	}

	public boolean getVisiblility(){ 
		//yes i butchered the spelling, but i butcher it everywhere
		return isVisible;
	}

	/*
	 * This draws the image the way that it is assumed, as the coordanites in the center
	 */
	public void draw(Graphics g, ImageObserver io) {
		int drawX=(int)Math.round(x);
		int drawY=(int)Math.round(y);
		if (isVisible) {
			g.drawImage(img, drawX-height/2, drawY-width/2, width, height, io);
		}
	}

	/*
	 * Switches the MovingImage visibility to the opposite of what it is
	 * currently.
	 */
	public void swapVisibility() {
		isVisible = !isVisible;
	}

	/*
	 * Returns the visibility of the MovingImage.
	 */
	public boolean isVisible() {
		return isVisible;
	}

	/*
	 * Returns true if the x,y point passed in is inside of this image.
	 * (For example, to check and see if this image was clicked on)
	 */
	public boolean isPointInside(double x, double y) {
		this.x-=this.width/2;
		this.y-=this.height/2;
		if (x >= this.x && x <= this.x + width && y >= this.y && y <= this.y + height){
			this.x+=this.width/2;
			this.y+=this.height/2;
			return true;
		}
		this.x+=this.width/2;
		this.y+=this.height/2;
		return false;
	}

	//so usefull, it should be included in the default :D
	public boolean isInside(MovingImage other){
		//this random stuff is because the point (x,y) is the CENTER point
		//not the corner...
		this.x-=this.width/2;
		this.y-=this.height/2;
		other.x-=other.width/2;
		other.y-=other.height/2;
		if(x<other.x+other.width&&x+width>other.x&&y<other.y+other.height&&y+height>other.y){
			this.x+=this.width/2;
			this.y+=this.height/2;
			other.x+=other.width/2;
			other.y+=other.height/2;
			return true;
		}
		this.x+=this.width/2;
		this.y+=this.height/2;
		other.x+=other.width/2;
		other.y+=other.height/2;
		return false;
	}
	
	public Rectangle2D.Double getBoundingRect(){
		return new Rectangle2D.Double(x-width/2,y-height/2,width,height);
	}
	
	public Line2D.Double[] getBoundingLines(){
		Line2D.Double[] out=new Line2D.Double[4];
		double xCorn=x-width/2;
		double yCorn=y-width/2;
		out[0]=new Line2D.Double(xCorn,yCorn,xCorn+width,yCorn);
		out[1]=new Line2D.Double(xCorn,yCorn,xCorn,yCorn+height);
		out[2]=new Line2D.Double(xCorn+width,yCorn,xCorn+width,yCorn+height);
		out[3]=new Line2D.Double(xCorn,yCorn+height,xCorn+width,yCorn+height);
		return out;
	}

}


