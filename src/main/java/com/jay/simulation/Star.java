/*
 * **************************************************************************
 * Copyright © 2011-2014 Jay Kamat
 *
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 *
 * This file is part of JPlanet Simulator.
 *
 * JPlanet Simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPlanet Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JPlanet Simulator.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package com.jay.simulation;

import java.awt.Image;

import com.dres.util.ImageLoader;
import com.jay.PlanetSim;
import com.jay.graphics.MovingImage;

/**
 * Jay Kamat
 * Period 5
 * Version 2.3
 *
 * if you want a full reason why stars dont move, ASK ME
 * and if you are my future self then you really are stupid (ask your brain)
 */
public class Star extends MovingImage {

    //pretty simple class, but essential for the program
    //well SORT OF AT LEAST
    private static final Image pictures[]={ImageLoader.load("/images/sun.png")};
    private double mass;
    public static final int defaultStarWidth=40;
    public static final int defaultStarHeight=40;
    public static final int defaultStarMass=300;

    public Star(int xs,int ys,int massx){
        super(null,xs,ys,
                defaultStarWidth,defaultStarHeight);
        mass=massx;
        setUpImages();
    }

    public Star(PlanetSim window, int xs,int ys){
        super(null,xs,ys,
                defaultStarWidth,defaultStarHeight);
        mass=window.panel2.getSMass();
        setUpImages();
    }

    private void setUpImages(){
        this.setImage(pictures[(int)Math.random()*(pictures.length-1)]);
    }

    public double getMass(){
        return mass;
    }

    @Override
    public String toString(){
        return("("+x+","+y+") Mass="+mass);
    }
}
//well in hindsight, i should have done this
//movingImage<--star<--planet<--spaceship, but i just had to complicate things
