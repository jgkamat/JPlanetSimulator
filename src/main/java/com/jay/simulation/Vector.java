/*
 * **************************************************************************
 * Copyright © 2011-2014 Jay Kamat
 * 
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 * 
 * This file is part of JPlanet Simulator.
 * 
 * JPlanet Simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPlanet Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JPlanet Simulator.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package com.jay.simulation;

import java.awt.Graphics;
import java.awt.geom.Line2D;

import com.jay.graphics.MovingImage;


/**
 * Jay Kamat
 * Period 5
 * Version 3.2
 */
public class Vector{
	//this is the CORE of the program
	//the basis for ALL calculations
	//lets just say removing this would be hard

	private double x1,y1,x2,y2;
	private boolean point; //states if the vector is just a point, or a vector with no length

	public Vector(double x1s,double y1s,double x2s,double y2s){ //starting coords is x1,y1 end coords is x2,y2
		x1=x1s;
		x2=x2s;
		y1=y1s;
		y2=y2s;
		if(x1==x2&&y1==y2){
			point=true;
		}
		else point=false;
	}

	public Vector(double x1s,double y1s,double x2s,double y2s,double distance){ //uses points as direction, and distance...
		double xn;
		double yn;
		double angle;//below creates an angle based on polar theta
		angle=Math.atan2((y2s-y1s),(x2s-x1s)); 
		xn=distance*Math.cos(angle);
		yn=distance*Math.sin(angle); //before:yn=-distance*Math.sin(angle);?
		x1=x1s;
		y1=y1s;

		x2=x1+xn;
		y2=y1+yn;

		if(x1==x2&&y1==y2){
			point=true;
		}
		else point=false;
	}

	//CONSTRUCTOR IS BUGGED. USE THE ACEMAKER VERSION
	public Vector(double x1s,double x2s, double angle, int distance, boolean justToMakeConstructorsDifferent){
		double xn;
		double yn;

		xn=distance*Math.cos(angle);
		yn=distance*Math.sin(angle);

		x2=x1+xn;
		y2=y1+yn;

		if(x1==x2&&y1==y2){
			point=true;
		}
		else point=false;
	}

	public void resultant(Vector other){  //finds the resultant between 2 vectors and puts result in first
		//they must start from the same point
		double xSet;
		double ySet;
		xSet=this.getX()+other.getX();
		ySet=this.getY()+other.getY();
		x2=x1+xSet;
		y2=y1+ySet;
	}

	public boolean isPoint(){
		return point;
	}

	public void allResultant(Vector[] other){ //take an array of vectors and set result to first
		for(int counter=0;counter<other.length;counter++){
			if(other[counter]!=null&&this!=null)
				this.resultant(other[counter]);
		}
	}

	public double getLength(){
		if(point==true){
			return 0;
		}
		return (Math.hypot(getX(),getY()));
	}

	public double getX(){ //gets the x COMPONENT of the vector
		return x2-x1;
	}

	public double getY(){ //gets the y COMPONENT of the vector
		return y2-y1;
	}

	public double getX1(){
		return x1;
	}

	public double getY1(){
		return y1;
	}

	public double getY2(){
		return y2;
	}

	public double getX2(){
		return x2;
	}

	public double getDirection(){   //finds direction angle in degrees WITHOUT Y FLIPPED
		return ((Math.atan2(getY(),getX()))); 
	}

	public void set(double x1s,double y1s,double x2s,double y2s){//if you want to be technical
		x1=x1s;
		x2=x2s;
		y1=y1s;
		y2=y2s;
		if(x1==x2&&y1==y2){
			point=true;
		}
		else point=false;
	}

	public Vector setDistance(double distance){
		return new Vector(x1,y1,x2,y2,distance);
	}

	public double getDistance(){//gets the travelling distance
		return (Math.hypot((x2-x1),(y2-y1)));
	}

	public static double getDistance(double x1s, double y1s, double x2s, double y2s){
		//gets traveling distance for any coordanates
		return (Math.hypot((x2s-x1s),(y2s-y1s)));
	}

	public static double getAngle(double x1s, double y1s, double x2s, double y2s){
		//returns the angle between two points (0-2pi)
		return Math.atan2(y2s-y1s,x2s-x1s);
	}

	public boolean intersects(Vector other){
		return Line2D.linesIntersect(x1, y1, x2, y2, other.getX1(),other.getY1(),other.getX2(),other.getY2());
	}

	public boolean intersects(double x1s,double y1s,double x2s,double y2s){
		return Line2D.linesIntersect(x1, y1, x2, y2, x1s,y1s,x2s,y2s);
	}

	public boolean intersects(Line2D other){
		return Line2D.linesIntersect(x1, y1, x2, y2, other.getX1(),other.getY1(),other.getX2(),other.getY2());
	}
	
	public boolean intersects(MovingImage other){
		if(Line2D.linesIntersect(x1, y1, x2, y2, other.getXC(),other.getY(),other.getXC()+other.getWidth(),other.getYC()))
			return true;
		if(Line2D.linesIntersect(x1, y1, x2, y2, other.getXC(),other.getY(),other.getXC(),other.getYC()+other.getHeight()))
			return true;
		/*
		if(Line2D.linesIntersect(x1, y1, x2, y2, other.getXC()+other.getWidth(),other.getY(),other.getXC()+other.getWidth(),other.getYC()+other.getHeight()))
			return true;
		if(Line2D.linesIntersect(x1, y1, x2, y2, other.getXC(),other.getY()+other.getHeight(),other.getXC()+other.getWidth(),other.getYC()+other.getHeight()))
			return true;
			*/
		return false;
	}

	@Override
	public String toString(){
		//well...what would I do?
		return ("("+x1+","+y1+") ("+x2+","+y2+")");
	}

	public void draw(Graphics g){
		//so simple its sweet :D
		g.drawLine((int)Math.round(x1), (int)Math.round(y1), (int)Math.round(x2), (int)Math.round(y2));
	}

}
