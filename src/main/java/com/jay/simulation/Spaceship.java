/*
 * **************************************************************************
 * Copyright © 2011-2014 Jay Kamat
 *
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 *
 * This file is part of JPlanet Simulator.
 *
 * JPlanet Simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPlanet Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JPlanet Simulator.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package com.jay.simulation;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.ImageObserver;

import com.dres.util.ImageLoader;
import com.jay.PlanetSim;


/**
 * Jay Kamat
 * Period 5
 * Version 0.6
 */
public class Spaceship extends Planet { //well...the spaceship moves...but it aint a planet
	//the planet class should really just be named "satillite" but planet sounds easy for the kiddies

	private static Image on = ImageLoader.load("/images/spaceshipon.png"),
						off= ImageLoader.load("/images/spaceshipoff.png");
	public static final int defaultSpaceshipMass=10, defaultSpaceshipThrust=1;
	private Vector starForces[]=new Vector[100];  //forces due to gravity from stars
	private Vector thrust;
	double rotation=0;
	private float dRot;
	private PlanetSim window;
	public static final float defaultDRot= 0.2f;

	public Spaceship(PlanetSim windowx, int xs, int ys){
		super(windowx,xs,ys,50,50,defaultSpaceshipMass,null);
		window=windowx;
		this.setImage(off);
		window.panel2.resetFuel();
		window.panel2.resetScore();
		dRot=0;
	}

	public Vector getThrust(){
		return thrust;
	}

	public void startThrust(){
		if(window.panel2.fuel!=0){
			thrust=new Vector(x,y,rotation-Math.PI/2,Spaceship.defaultSpaceshipThrust,true);
			setImage(on);
		}
	}

	public void stopThrust(){
		thrust=null;
		setImage(off);
	}

	public void rotate(double amount){ //rotates this much clockwise IN RADIANS
		//<3 the simplicity
		rotation-=amount;
		if(thrust!=null) {
			thrust=new Vector(x,y,rotation-Math.PI/2,2,true);
		}
	}

	//feul stuff... really it is so self explaintory...

	public int getFuel(){
		return window.panel2.fuel;
	}

	public void depleteFuel(int i){
		if(window.panel2.fuel==0)
			return;
		window.panel2.fuel-=i;
	}

	public void depleteFuel(){
		if(window.panel2.fuel==0){
			stopThrust();
			return;
		}
		window.panel2.fuel-=2;
		window.panel2.refreshFuel();
	}

	@Override
	public void draw(Graphics gs, ImageObserver io) {
		//"you dont need to know what a affineTransform is"- Mr. Shelby (but i WANT to know :P)
		AffineTransform holder;
		Graphics2D g = (Graphics2D)gs;
		holder=g.getTransform();
		g.rotate(rotation,x,y);

		super.draw(gs, io);

		g.setTransform(holder);
	}

	public void setDRot(float in){
		dRot=in;
	}

	public float getDRot(){
		return dRot;
	}



	public void act(){
		//well...this looks awfully familliar
		//it's just the planet's refresh method...there are just a few miner changes

		for(int i=0;i<starForces.length;i++){
			starForces[i]=null;
		}

		for(int counter=0;counter<stars.length;counter++){ //star-ship
			if(stars[counter]==null)
				continue;
			double distance=((window.panel2.getG()*stars[counter].getMass()*mass)/
					Math.pow(Vector.getDistance(x, y, stars[counter].getX(),
							stars[counter].getY()),2));

			starForces[counter]=new Vector(x,y,stars[counter].getX(),stars[counter].getY(),
					distance);

			if(thrust!=null){
				depleteFuel();
			}
			this.rotate(dRot);
		}

		//force!=acceleration update
		Vector force=new Vector(speed.getX1(),speed.getY1(),speed.getX2(),speed.getY2());
		force.allResultant(starForces);
		force.setDistance(force.getDistance()/this.mass);
		speed=force;

		//speed.allResultant(starForces);


		//more collision detection, for high speeds only for stars
		if(thrust!=null)
			speed.resultant(thrust);
		if(speed.getDistance()>20){ //sets a speed limit so the user isnt freaked out
			//without this, you could keep accelerating to infinity m/s
			//speed=speed.setDistance(20);
		}
		if(speed.getDistance()>20&&altAdvColl){
			double times=speed.getDistance()/20.0;
			double xIncrease=speed.getX()/times;
			double yIncrease=speed.getY()/times;
			double xAt=speed.getX1();
			double yAt=speed.getY1();

			//well...might as well
			for(int counter=0;counter<times-1;counter++){
				xAt+=xIncrease;
				yAt+=yIncrease;
				for(int counter2=0;counter2<stars.length;counter2++){
					if(stars[counter2]==null)
						continue;
					if(stars[counter2].isPointInside(xAt, yAt)&&window.panel!=null){
						window.panel.killShip();
						break;
					}
				}
			}
		}
		//moves to the resultant
		this.moveTo(speed.getX2(),speed.getY2());
		//keeps the planet's inertia by sticking it into a vector
		this.setSpeed(new Vector(speed.getX2(),speed.getY2(),
				speed.getX2()+speed.getX(),speed.getY2()+speed.getY()));

		/*
		 * This is extremely similar to the planet calculation method, go look at that!
		 */
	}
}
