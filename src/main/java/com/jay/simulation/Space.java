/*
 * **************************************************************************
 * Copyright © 2011-2014 Jay Kamat
 *
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 *
 * This file is part of JPlanet Simulator.
 *
 * JPlanet Simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPlanet Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JPlanet Simulator.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package com.jay.simulation;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import com.dres.util.ImageLoader;
import com.jay.PlanetSim;
import com.jay.graphics.MovingImage;

/*
 * Jay Kamat
 * Period 5
 *
 * Version 2.6
 */
public class Space extends JPanel implements MouseListener,MouseMotionListener,KeyListener, Runnable {

	private static final long serialVersionUID = 6312556650367965385L;
	private int width;
	private int height;
	//the limit for planets and stars is 100
	//if you want more, you will need to edit that 100 everywhere (planet)
	private Star[] stars=new Star[100];
	private Planet[] planets=new Planet[100];
	public static int ignore=-1;
	//just the planet not to move (the one the user is making)
	private int gameMode, planetCounter=0;
	private int xPress,yPress,xMove,yMove;
	private static Spaceship ship;
	private boolean drawLine=false;
	private boolean paused=false;
	private final Image background=ImageLoader.load("/images/background.jpg");
	private final Image pausedImage=ImageLoader.load("/images/paused.png");
	private boolean startStar=true;

	//Keybindings
	public static int
	kUp = KeyEvent.VK_UP, kUp2 = KeyEvent.VK_W,
	kLeft = KeyEvent.VK_LEFT, kLeft2 = KeyEvent.VK_A,
	kRight = KeyEvent.VK_RIGHT, kRight2 = KeyEvent.VK_D;
	private PlanetSim window;

	public Space(PlanetSim windowx){
		super();

		window=windowx;

		Planet.giveStars(stars);
		Planet.givePlanets(planets);

		listenerBoot();

	}

	private void listenerBoot(){
		addMouseListener(this);
		addMouseMotionListener(this);
		addKeyListener(this);
	}

	public void refreshGM(){
		gameMode=window.getGameMode();
		destroyAll();
		killShip();
	}

	public void destroyAll(){
		for(int counter=0;counter<planets.length;counter++){
			if(counter!=ignore) //dont remove a planet that the user is placing
				planets[counter]=null;
		}
		for(int counter=0;counter<stars.length;counter++){
			stars[counter]=null;
		}
	}

	public void killMe(int where){ //allows a planet to kill itself (suicidal tendencies)
		planets[where]=null;
	}

	public void killShip(){
		ship=null;
		destroyAll();

	}

	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);

		width = getWidth();
		height = getHeight();

		g.drawImage(background,0,0,width,height,this);
		g.setColor(Color.RED);

		MovingImage.setSize(width,height);

		//draws stars
		for(int counter=0;counter<stars.length;counter++){
			if(stars[counter]!=null){
				stars[counter].draw(g,this);
			}
		}
		//draws/moves planets
		for(int counter=0;counter<planets.length;counter++){
			if(planets[counter]!=null){
				if(counter!=ignore&&!paused)
					planets[counter].refresh(counter);
				//refresh is where it all happens (check in space class)
				//it moves the planet

				if((planets[counter]!=null))
					planets[counter].draw(g,this);

			}
		}
		//draws vectors in edu mode
		if(window.panel2.getEduMode()){
			window.panel.drawVectors(g);
		}
		//draws the lovely line showing where you "throw" the planet
		if(drawLine){
			g.drawLine(xPress,yPress,xPress+(xPress-xMove),yPress+(yPress-yMove));
		}
		if(gameMode==2){
			if(ship!=null)
				ship.draw(g, this);
		}

		//paused screen
		if(paused){
			g.drawImage(pausedImage,0,0,width,height,this);
		}

	}
	//normal collision detection, more advanced is in the refresh method of the planet class
	//collision detection (star-planet):
	public void collisionDetection(){
		for(int counter=0;counter<planets.length&&planets[counter]!=null;counter++){
			for(int counter2=0;counter2<stars.length&&stars[counter2]!=null;counter2++){
				if(stars[counter2]!=null&&planets[counter]!=null&&
						ignore!=counter&&planets[counter].isInside(stars[counter2])){
					planets[counter]=null;
				}
			}
		}
		//collision detection (planet-planet), this is only when planet-gravity is off
		if(window.panel2.getPlanetGravity()){
			for(int counter=0;counter<planets.length&&gameMode==1;counter++){
				for(int counter2=0;counter2<planets.length;counter2++){
					if(planets[counter]!=null&&planets[counter2]!=null&&counter2!=counter
							&&counter!=ignore&&counter2!=ignore){
						if(planets[counter].isInside(planets[counter2])){
							planets[counter]=null;
							planets[counter2]=null;
						}
					}
				}
			}
		}

		//user crashes
		//planets
		for(int counter=0;counter<planets.length&&gameMode==2;counter++){
			if(planets[counter]!=null&&ship!=null){
				if(ship.isInside(planets[counter])){
					planets[counter]=null;
					killShip();
				}
			}
		}
		//stars
		for(int counter=0;counter<stars.length&&gameMode==2;counter++){
			if(stars[counter]!=null&&ship!=null){
				if(ship.isInside(stars[counter])){
					killShip();
				}
			}
		}

	}

	//formula used F=mv^2/r or sqrt(Fr/m)=v
	//MORE PHYSICS DONT YOU LOVE IT <3
	private void spawnPlanet(){
		if(ship==null){
			ship=new Spaceship(window,width*2/3,height-Planet.defaultPlanetHeight/2);
			return;
		}

		int x=width/2;
		int y=(int)(Math.random()*(height/2-50));
		if(ship.isPointInside(x, y)){
			spawnPlanet();
			//RECURSIONNNN!!!! not really :P
			return;
		}
		//dont spawn stuff inside the ship
		double f=((window.panel2.getG()*window.panel2.getPMass()*window.panel2.getSMass())/
				Math.pow(Vector.getDistance(x, y, (int)stars[0].getX(),
						(int)stars[0].getY()),2));
		double r=Vector.getDistance(x, y, (int)stars[0].getX(),
				(int)stars[0].getY());

		//TODO fix this workaround!
		double m=Planet.defaultPlanetMass/10;
		double speed=-Math.sqrt(f*r/m);
		Vector magnitude=new Vector(x,y,(int)(x+speed),y);
		for(int counter=0;counter<planets.length;counter++){
			if(planets[counter]==null){
				planets[counter]=new Planet(window,x,y);
				planets[counter].setSpeed(magnitude);
				break;
			}
		}
		refreshCount();
	}

	public void drawVectors(Graphics g){
		for(int i=0;i<planets.length;i++){
			if(planets[i]!=null){
				g.setColor(Color.yellow);
				for(int j=0;planets[i]!=null&&j<planets[i].getStarForces(i).length;j++){
					if(planets[i].getStarForces(i)[j]!=null) {
						planets[i].getStarForces(i)[j].setDistance(planets[i].getStarForces(i)[j].getDistance()*5).draw(g);
					}
				}
				if(planets[i]==null)
					break;
				g.setColor(Color.WHITE);
				for(int j=0;planets[i]!=null&&j<planets[i].getPlanetForces(i).length;j++){
					if(planets[i]!=null&&planets[i].getPlanetForces(i)[j]!=null
							&&planets[i].getPlanetForces(i)[j].getLength()>=1) {
						planets[i].getPlanetForces(i)[j].setDistance(planets[i].getPlanetForces(i)[j].getDistance()*5).draw(g);
					}
				}

				if(planets[i]==null)
					break;
				g.setColor(Color.red);
				planets[i].getSpeed().setDistance(planets[i].getSpeed().getDistance()*5).draw(g);
				if(planets[i]==null)
					break;
			}
		}
	}

	private void refreshCount(){
		int star=0, planet=0;
		for(int counter=0;counter<stars.length;counter++){
			if(stars[counter]!=null)
				star++;
		}
		for(int counter=0;counter<planets.length;counter++){
			if(planets[counter]!=null)
				planet++;
		}
		window.panel2.setStarCount(star);
		window.panel2.setPlanetCount(planet);
	}


	//where we start everything :D
	public void run(){
		long timer;
		for(;;){
			timer=System.currentTimeMillis();
			if(paused==false){
				collisionDetection();
				//default star, IS resizable (scalable) so weird things DO happen when resizing
				//thats why my window is fullscreen/locked...evil me
				if(window.panel2.getStartStar())
					stars[0] = new Star(window, width/2,height/2);

				if(gameMode==2&&paused==false){

					planetSpawner();

					if(ship!=null){
						ship.act();

						window.panel2.score++;
						window.panel2.refreshScore();
					}
				}

				refreshCount();
			}

			if(window.getGameMode()!=PlanetSim.CREATIVE||
					window.getGameMode()!=PlanetSim.SURVIVAL)
				window.repaint();
			else
				this.repaint();

			try{
				Thread.sleep(Math.max(window.panel2.delay-(System.currentTimeMillis()-timer),1));
				//well...no number better than 30
			} catch (InterruptedException e) {}
		}
	}

	private void planetSpawner(){
		planetCounter++;
		if(planetCounter>=50){
			spawnPlanet();
			planetCounter=0;
		}
	}
	//IMPLEMENTED METHODS

	public void mouseClicked(MouseEvent e) {
		/*if(e.getButton()==MouseEvent.BUTTON1){
            for(int counter=0;counter<planets.length;counter++){
                if (planets[counter]==null){
                    planets[counter]=new Planet(e.getX(), e.getY());
                    planets[counter].setSpeed(new Vector(e.getX(),e.getY(),e.getX(),e.getY()-20));
                    break;
                }
            }
        }*/
	}

	public void mousePressed(MouseEvent e) {
		//for the release
		xPress=e.getX();
		yPress=e.getY();

		//records where you click, and adds a immovable planet
		if(e.getButton()==MouseEvent.BUTTON1&&gameMode==1){
			for(int counter=0;counter<planets.length;counter++){
				if (planets[counter]==null){
					planets[counter]=new Planet(window,e.getX(), e.getY());
					ignore=counter;
					break;
				}
			}
		}

		if(e.getButton()==MouseEvent.BUTTON3&&gameMode==1){
			//adds stars
			for(int counter=0;counter<stars.length;counter++){
				if (stars[counter]==null){
					stars[counter]=new Star(window, e.getX(), e.getY());
					break;
				}
			}
		}
		else if(e.getButton()==MouseEvent.BUTTON2&&gameMode==1){
			//deletes stuff
			for(int counter=0;counter<stars.length;counter++){
				if(stars[counter]!=null&&stars[counter].isPointInside(e.getX(), e.getY())){
					stars[counter]=null;
				}
			}
			for(int counter=0;counter<planets.length;counter++){
				if(planets[counter]!=null&&planets[counter].isPointInside(e.getX(), e.getY())){
					planets[counter]=null;
				}
			}
		}
	}

	public void mouseReleased(MouseEvent e) {
		//finalizes the planet, adds your velocity
		if(e.getButton()==MouseEvent.BUTTON1&&gameMode==1){
			if((xPress!=e.getX()||yPress!=e.getY())&&ignore!=-1){
				planets[ignore].setSpeed(new Vector(xPress,yPress,
						xPress+(xPress-e.getX())/5,yPress+(yPress-e.getY())/5));
			}
			ignore=-1;
			drawLine=false;
		}
	}

	public void mouseEntered(MouseEvent e) {
		//unpauses the game/gets stuff from the inputs
		paused=false;

		setFocusable(true);
		requestFocusInWindow();

		if(startStar&&!window.panel2.getStartStar()){
			stars[0]=null;
		}
		startStar=window.panel2.getStartStar();
	}

	public void mouseExited(MouseEvent e) {
		//pauses the game (and allows editing of fields)
		paused=true;
	}

	public void mouseDragged(MouseEvent e) {
		//allows the lovely red line to be drawn when moving the mouse
		if(ignore!=-1&&gameMode==1){ //i have no idea why e.getButton()==MouseEvent.BUTTON1 not works
			drawLine=true;
			xMove=e.getX();
			yMove=e.getY();
		}
		//repaint gets called in the run method so not needed here
	}

	public void mouseMoved(MouseEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void keyPressed(KeyEvent e) {
		//allows you to destroy everything with the escape key
		if(e.getKeyCode()==KeyEvent.VK_ESCAPE&&gameMode==1){
			destroyAll();
		}
		if(e.getKeyCode()==kUp||e.getKeyCode()==kUp2
				&&gameMode==2&&!paused&&ship!=null){
			ship.startThrust();
		}
		if(e.getKeyCode()==kLeft||e.getKeyCode()==kLeft2
				&&gameMode==2&&!paused&&ship!=null){
			ship.setDRot(Spaceship.defaultDRot);
		}
		if(e.getKeyCode()==kRight||e.getKeyCode()==kRight2
				&&gameMode==2&&!paused&&ship!=null){
			ship.setDRot(-Spaceship.defaultDRot);
		}

		if(e.getKeyCode()==KeyEvent.VK_F5){
			this.repaint();
		}
	}

	public void keyReleased(KeyEvent e) {
		if(ship!=null&&(e.getKeyCode()==kUp||e.getKeyCode()==kUp2)&&gameMode==2){
			ship.stopThrust();
		}
		if(gameMode==2&&ship!=null&&(e.getKeyCode()==kRight||e.getKeyCode()==kRight2
			||e.getKeyCode()==kLeft||e.getKeyCode()==kLeft2)){
			ship.setDRot(0);
		}
	}
}
