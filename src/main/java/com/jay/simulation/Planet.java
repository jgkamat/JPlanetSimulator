/*
 * **************************************************************************
 * Copyright © 2011-2014 Jay Kamat
 *
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 *
 * This file is part of JPlanet Simulator.
 *
 * JPlanet Simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPlanet Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JPlanet Simulator.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package com.jay.simulation;

import java.awt.Image;

import com.dres.util.ImageLoader;
import com.jay.PlanetSim;
import com.jay.graphics.MovingImage;

/**
 * Jay Kamat
 * Period 5
 * Version 2.6
 */
public class Planet extends MovingImage {
	//my pride and joy :D

	private static Image pictures[]={
		ImageLoader.load("/images/earth.png"),
		ImageLoader.load("/images/jupiter.png"),
		ImageLoader.load("/images/mars.png"),
		ImageLoader.load("/images/pluto.png"),
		ImageLoader.load("/images/saturn.png")};
	protected double mass;
	protected Vector speed;  //inertia/speed/velocity...just the current speed/direction
	private Vector starForces[]=new Vector[100];  //forces due to gravity from stars
	private Vector planetForces[]=new Vector[100];  //forces due to gravity from other planets
	protected static boolean altAdvColl=false,advColl=true;
	//if on, may kill planets too early(just a illusion), if off, may not kill planets at high speed
	protected static double advCollThreshold,advPlanetColThreshold;
	//it only works on star-planet
	protected static Star stars[];
	protected static Planet planets[];
	//hold the current stars/planets on the screen
	public static final int defaultPlanetWidth=10;
	public static final int defaultPlanetHeight=10, defaultPlanetMass=10;
	private PlanetSim window;

	public Planet(PlanetSim windowx, double xs,double ys,int massx){
		super(null,xs,ys,
				defaultPlanetWidth,defaultPlanetHeight);
		window=windowx;
		mass=massx;
		speed=new Vector(x,y,x,y);
		setUpImages();
		advCollThreshold=Math.min(Star.defaultStarHeight, Star.defaultStarWidth);
		advPlanetColThreshold=advCollThreshold/2;
	}

	public Planet(PlanetSim windowx,double xs,double ys,int widthx, int heightx,int massx, Image imge){
		super(imge,xs,ys,widthx,heightx);
		window=windowx;
		mass=massx;
		speed=new Vector(x,y,x,y);
		advCollThreshold=Math.min(Star.defaultStarHeight, Star.defaultStarWidth);
		advPlanetColThreshold=advCollThreshold/2;
	}

	public Planet(PlanetSim windowx,double xs,double ys){
		super(null,xs,ys,20,20);
		window=windowx;
		mass= window.panel2.getPMass();
		speed=new Vector(x,y,x,y);
		setUpImages();
		advCollThreshold=Math.min(Star.defaultStarHeight, Star.defaultStarWidth);
		advPlanetColThreshold=advCollThreshold/2;
	}

	private void setUpImages(){
		this.setImage(pictures[(int)(Math.random()*(pictures.length))]);
	}

	public Image getPic(){
		return super.getImage();
	}

	public double getMass(){
		return mass;
	}

	public static void giveStars(Star[] one){
		stars=one;
	}

	public static void givePlanets(Planet[] one){
		planets=one;
	}

	/*public static void addStar(int x,int y){
        for(int counter=0;counter<Array.getLength(stars);counter++){
            if(stars[counter]==null){
                stars[counter]=new Point(x,y);
                break;
            }
        }
    }

    public static void removeStar(int x,int y){ //coords must be same as inputed above
        for(int counter=0;counter<Array.getLength(stars);counter++){
            if(stars[counter].getX()==x&&stars[counter].getY()==y){
                stars[counter]=null;
                break;
            }
        }
    }*/

	public Vector[] getStarForces(int position){
		for(int counter=0;counter<stars.length;counter++){ //star-planet
			if(stars[counter]==null) {
				continue;
			}
			if(Vector.getDistance(x, y, stars[counter].getX(),
					stars[counter].getY())==0){
				window.panel.killMe(position);
				break;
			}
			double distance=((window.panel2.getG()*stars[counter].getMass()*mass)/
					Math.pow(Vector.getDistance(x, y, stars[counter].getX(),
							stars[counter].getY()),2));
			starForces[counter]=new Vector(x,y,stars[counter].getX(),stars[counter].getY(),
					distance);
		}
		return starForces;
	}

	public Vector[] getPlanetForces(int position){
		for(int counter=0;counter<planets.length&&window.panel2.getPlanetGravity();counter++){ //planet-planet
			if (counter==position||planets[counter]==null||counter==Space.ignore)
				continue;
			if(Vector.getDistance(x, y,
					planets[counter].getX(), planets[counter].getY())<=
					Planet.advPlanetColThreshold){
				//if the centers are close enough so the collisions is appealing
				window.panel.killMe(counter);
				window.panel.killMe(position);
				break;
			}
			if (planets[counter]==null||counter==Space.ignore)
				continue;
			double distance=(window.panel2.getG()*planets[counter].mass*mass/
					(Math.pow(Vector.getDistance(x, y,
							planets[counter].getX(), planets[counter].getY()),2)));
			planetForces[counter]=new Vector(x,y,planets[counter].getX(),planets[counter].getY(),
					distance);
		}
		return planetForces;
	}

	public void setStarForces(Vector[] other){
		starForces=other;
	}

	public void setPlanetForces(Vector[] other){
		planetForces=other;
	}

	public void setVectorStar(int number,Vector other){ //sets the vector starForces
		starForces[number]=other;
	}

	public void setVectorPlanet(int number,Vector other){ //sets the vector starForces
		planetForces[number]=other;
	}

	public void setSpeed(Vector other){  //sets the inertia vector
		speed=other;
	}

	public Vector getSpeed(){  //gets the inertia vector
		return speed;
	}

	/* public void addVector(Vector other){  //adds another vector (force) to this planet (from a sun)
        for(int counter=0;counter<Array.getLength(starForces);counter++){
            if(starForces[counter]==null){
                starForces[counter]=other;
                break;
            }
        }
    }*/ //new style code does not need this, passed in an array (by refrence)
	public void refresh(int position){  //takes the vectors you have added and refreshes the force
		//now also provides some collision detection
		//formula used: <<(G*m1*m2)/(r^2)>>
		//Translation: (some number*mass1*mass2)/distance between masses
		for(int i=0;i<starForces.length;i++){
			starForces[i]=null;
		}

		for(int i=0;i<planetForces.length;i++){
			planetForces[i]=null;
		}


		//gets all the planet forces/star forces in a vector array
		starForces=getStarForces(position);
		planetForces=getPlanetForces(position);

		//force !=acceleration update
		Vector force=new Vector(speed.getX1(),speed.getY1(),speed.getX2(),speed.getY2());
		force.allResultant(starForces);
		force.allResultant(planetForces);
		force.setDistance(force.getDistance()/this.mass);

		speed=force;

		//not entirely accurate force-code
		//speed.allResultant(starForces);
		//speed.allResultant(planetForces);

		//more collision detection, for high speeds <only for stars>

		if(advColl&&(speed.getDistance()>advCollThreshold)){
			double times=speed.getDistance()/20.0;
			double xIncrease=speed.getX()/times;
			double yIncrease=speed.getY()/times;
			double xAt=speed.getX1();
			double yAt=speed.getY1();

			for(int counter=0;counter<times-1;counter++){
				xAt+=xIncrease;
				yAt+=yIncrease;
				for(int counter2=0;counter2<stars.length;counter2++){
					if(stars[counter2]==null)
						continue;
					if(stars[counter2].isPointInside(xAt, yAt)){
						window.panel.killMe(position);
						break;
					}
				}
			}
		}

		if(altAdvColl&&speed.getDistance()>advCollThreshold){
			for(Star target:stars){
				if(target==null)
					continue;
				if(speed.intersects(target)){
					window.panel.killMe(position);
					break;
				}
			}
		}


		//moves to the resultant
		this.moveTo(speed.getX2(),speed.getY2());
		//keeps the planet's inertia by sticking it into a vector

		this.setSpeed(new Vector(speed.getX2(),speed.getY2(),
				speed.getX2()+speed.getX(),speed.getY2()+speed.getY()));

		/*
		 * if this is doesn't make any sense (I doubt it)
		 *
		 * 1:calculate all forces on the planet. stick em' into the vector
		 * 2:take the resultant of each of the forces and the current speed
		 * 3:stick the answer back into the speed
		 * 4:repeat for all forces
		 * 5:move the planet to where the speed points to
		 * 6:set the speed to the speed in <5>, except that it starts from the new planet
		 * this keeps a planets inertia
		 * (if you are still confused ask me (i doubt this possibility even more))
		 * (if you are my future self: you have become very stupid, go learn physics again)
		 */
	}

	@Override
	public String toString(){
		//might as well...
		return("("+x+","+y+") Mass="+mass);
	}
}
