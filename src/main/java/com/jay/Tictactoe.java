/*
 * **************************************************************************
 * Copyright © 2011-2014 Jay Kamat
 * 
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 * 
 * This file is part of JPlanet Simulator.
 * 
 * JPlanet Simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPlanet Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JPlanet Simulator.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package com.jay;

/*
 *Jay Kamat
 *Version 2.16
 *Plays a TicTacToe game......and........yah........er........ummmmmmmm
 *
 */
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;
import java.util.Scanner;
import javax.swing.*;

public class Tictactoe extends JPanel implements ActionListener, KeyListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2948187101378897342L;
	private static JButton button[][]=new JButton[3][3];
	private static JTextArea computer,user;

	int y;
	int x;
	int counter;
	int counter2;
	int counter3;
	int counter4;
	int grid;
	boolean extra;
	boolean win=false;
	boolean lose=false;
	boolean tie=false;
	boolean checker;
	int userScore=0;
	int compScore=0;
	final String winMessage=("");
	final String loseMessage=("");
	final String tieMessage=("");
	char board[][]=new char[3][3];//!!!!!THIS ARRAY IS [Y][X] NOT [X][Y] because i am stupid :(!!!!!
	Scanner s = new Scanner(System.in);
	Random ran;

	public void input(){ //handles input
		checker=false;
		while (checker==false){
			System.out.print("\nenter x cord. : ");
			x=s.nextInt();
			System.out.print("enter y cord. : ");
			y=s.nextInt();
			if(x>3||y>3){
				System.out.println("Less than 3 prease!");
				continue;
			}
			y--;
			x--;
			if(board[y][x]==0){
				checker=true;
			}
			else{
				System.out.println("Input a value in an empty square");
				checker=false;
			}
		}
	}

	public void changer(){ //input-->array
		board[y][x]='X'; //see above angry comment at the array...
	}
	public void checker(){ //checks for a win/loss
		counter=0;
		counter2=0;
		counter3=0;
		if (board[0][0]==board[0][1]&&board[0][1]==board[0][2]){
			if(board[0][0]=='X'){
				//System.out.println("\n"+winMessage);
				win=true;
				extra=false;
			}
			else if(board[0][0]=='O'){
				//System.out.println("\n"+loseMessage);
				lose=true;
				extra=false;
			}
		}
		else if (board[1][0]==board[1][1]&&board[1][1]==board[1][2]){
			if(board[1][0]=='X'){
				//System.out.println("\n"+winMessage);
				win=true;
				extra=false;
			}
			else if(board[1][0]=='O'){
				//System.out.println("\n"+loseMessage);
				lose=true;
				extra=false;
			}
		}
		else if (board[2][0]==board[2][1]&&board[2][1]==board[2][2]){
			if(board[2][0]=='X'){
				//System.out.println("\n"+winMessage);
				win=true;
				extra=false;
			}
			else if(board[2][0]=='O'){
				//System.out.println("\n"+loseMessage);
				lose=true;
				extra=false;
			}
		}
		else if (board[0][0]==board[1][0]&&board[1][0]==board[2][0]){
			if(board[0][0]=='X'){
				//System.out.println("\n"+winMessage);
				win=true;
				extra=false;
			}
			else if(board[0][0]=='O'){
				//System.out.println("\n"+loseMessage);
				lose=true;
				extra=false;
			}
		}
		else if (board[0][1]==board[1][1]&&board[1][1]==board[2][1]){
			if(board[0][1]=='X'){
				//System.out.println("\n"+winMessage);
				win=true;
				extra=false;
			}
			else if(board[0][1]=='O'){
				//System.out.println("\n"+loseMessage);
				lose=true;
				extra=false;
			}
		}
		else if (board[0][2]==board[1][2]&&board[1][2]==board[2][2]){
			if(board[0][2]=='X'){
				//System.out.println("\n"+winMessage);
				win=true;
				extra=false;
			}
			else if(board[0][2]=='O'){
				//System.out.println("\n"+loseMessage);
				lose=true;
				extra=false;
			}
		}
		else if (board[0][0]==board[1][1]&&board[1][1]==board[2][2]){
			if(board[0][0]=='X'){
				//System.out.println("\n"+winMessage);
				win=true;
				extra=false;
			}
			else if(board[0][0]=='O'){
				//System.out.println("\n"+loseMessage);
				lose=true;
				extra=false;
			}
		}
		else if (board[2][0]==board[1][1]&&board[1][1]==board[0][2]){
			if(board[0][2]=='X'){
				//System.out.println("\n"+winMessage);
				win=true;
				extra=false;
			}
			else if(board[0][2]=='O'){
				//System.out.println("\n"+loseMessage);
				lose=true;
				extra=false;
			}
		}//yes i could have made this shorter but i was stupid :(
		else if(board[0][0]!=0&&board[0][1]!=0&&board[0][2]!=0&&board[1][0]!=0&&board[1][1]!=0&&board[1][2]!=0
				&&board[2][0]!=0&&board[2][1]!=0&&board[2][1]!=0){
			//System.out.println("\n"+tieMessage);
			tie=true;
			extra=false;
		}
	}
	public void ai(){ //Computer's Brainz
		counter=0;
		counter2=0;
		counter3=0;
		counter4=0;
		char check='O';
		boolean assignedSpot=false;
		if(board[1][1]==0){
			board[1][1]='O';
			assignedSpot=true;
		}
		else{ //AI, checks for 2 of same, then tries to block or win
			for(;counter3<2;counter3++, check='X'){
				for(counter=0;counter<3;counter++){ //does "AI" for horizontal spaces
					if((board[counter][0]==board[counter][1]||board[counter][2]==
						board[counter][1]||board[counter][0]==board[counter][2])&&
						!(board[counter][0]!=0&&board[counter][1]!=0&&board[counter][2]!=0)&&
						!(board[counter][0]==0&&board[counter][1]==0&&board[counter][2]==0)&&
						assignedSpot==false){
						if(board[counter][0]==0&&board[counter][1]==check&&board[counter][2]==check){
							board[counter][0]='O';
							assignedSpot=true;
						}
						else if(board[counter][1]==0&&board[counter][2]==check&&board[counter][0]==check){
							board[counter][1]='O';
							assignedSpot=true;
						}
						else if(board[counter][2]==0&&board[counter][1]==check&&board[counter][0]==check){
							board[counter][2]='O';
							assignedSpot=true;
						}
					}
				}
				for(counter=0;counter<3;counter++){ //does "AI" for vertical spaces
					if((board[0][counter]==board[2][counter]||board[0][counter]==
						board[1][counter]||board[1][counter]==board[2][counter])&&
						!(board[0][counter]!=0&&board[1][counter]!=0&&board[2][counter]!=0)&&
						!(board[1][counter]==0&&board[2][counter]==0&&board[0][counter]==0)&&
						assignedSpot==false){
						if(board[0][counter]==0&&board[1][counter]==check&&board[2][counter]==check){
							board[0][counter]='O';
							assignedSpot=true;
						}
						else if(board[1][counter]==0&&board[2][counter]==check&&board[0][counter]==check){
							board[1][counter]='O';
							assignedSpot=true;
						}
						else if(board[2][counter]==0&&board[1][counter]==check&&board[0][counter]==check){
							board[2][counter]='O';
							assignedSpot=true;
						}
					}
				}
				if((board[0][0]==board[2][2]||board[0][0]== //diagonal 1
					board[1][1]||board[1][1]==board[2][2])&&
					!(board[0][0]!=0&&board[1][1]!=0&&board[2][2]!=0)&&
					!(board[1][1]==0&&board[2][2]==0&&board[0][0]==0)&&
					assignedSpot==false){
					if(board[0][0]==0&&board[1][1]==check&&board[2][2]==check){
						board[0][0]='O';
						assignedSpot=true;
					}
					else if(board[1][1]==0&&board[2][2]==check&&board[0][0]==check){
						board[1][1]='O';
						assignedSpot=true;
					}
					else if(board[2][2]==0&&board[1][1]==check&&board[0][0]==check){
						board[2][2]='O';
						assignedSpot=true;
					}
				}
				if((board[0][2]==board[2][0]||board[0][2]==  //diagonal 2
					board[1][1]||board[1][1]==board[2][0])&&
					!(board[0][2]!=0&&board[1][1]!=0&&board[2][2]!=0)&&
					!(board[1][1]==0&&board[2][2]==0&&board[0][0]==0)&&
					assignedSpot==false){
					if(board[0][2]==0&&board[1][1]==check&&board[2][0]==check){
						board[0][2]='O';
						assignedSpot=true;
					}
					else if(board[1][1]==0&&board[2][0]==check&&board[0][2]==check){
						board[1][1]='O';
						assignedSpot=true;
					}
					else if(board[2][0]==0&&board[1][1]==check&&board[0][2]==check){
						board[2][0]='O';
						assignedSpot=true;
					}
				}
			}
		}
		if(assignedSpot==false){
			int ranInt=ran.nextInt(4);
			if(ranInt==0&&board[0][0]==0){
				board[0][0]='O';
				assignedSpot=true;
			}
			else if(ranInt==1&&board[0][2]==0){
				board[0][2]='O';
				assignedSpot=true;
			}
			else if(ranInt==2&&board[2][2]==0){
				board[2][2]='O';
				assignedSpot=true;
			}
			else if(ranInt==3&&board[2][0]==0){
				board[2][0]='O';
				assignedSpot=true;
			}
		}
		if(assignedSpot==false){
			checker=false;
			while(checker==false){
				x=ran.nextInt(3);
				y=ran.nextInt(3);
				if (board[y][x]==0){
					board[y][x]='O';
					checker=true;
				}
			}
		}
	} //end of AI
	public void print(){ //prints out the board, adds _ where nothing is there
		counter=2;
		counter2=0;
		while(counter>=0){
			while(counter2<3){
				if(board[counter][counter2]==0){
					System.out.print('_');
				}
				else {

					System.out.print(board[counter][counter2]);
				}
				counter2++;
			}
			System.out.println();
			counter--;
			counter2=0;
		}
	}
	public Tictactoe(){
		ran=new Random();
		
		JFrame window = new JFrame("Tic Tac Toe");
		window.setSize(300, 150);
		window.setLocationRelativeTo(null);
		window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		this.setBackground(Color.CYAN);  // the default color is light gray
		JPanel b = new JPanel();
		JPanel c=new JPanel(new GridLayout(3,3));

		user=new JTextArea("User: 0",1,1);
		computer=new JTextArea("Computer: 0",1,1);
		user.setEditable(false);
		computer.setEditable(false);

		user.setFocusable(false);
		computer.setFocusable(false);

		button[0][0] = new JButton();
		button[0][0].setPreferredSize(new Dimension(80, 30));
		button[0][0].addActionListener(this);
		button[0][0].setFocusable(false);

		button[0][1] = new JButton();
		button[0][1].setPreferredSize(new Dimension(80, 30));
		button[0][1].addActionListener(this);
		button[0][1].setFocusable(false);

		button[0][2] = new JButton();
		button[0][2].setPreferredSize(new Dimension(80, 30));
		button[0][2].addActionListener(this);
		button[0][2].setFocusable(false);

		button[1][0] = new JButton();
		button[1][0].setPreferredSize(new Dimension(80, 30));
		button[1][0].addActionListener(this);
		button[1][0].setFocusable(false);

		button[1][1] = new JButton();
		button[1][1].setPreferredSize(new Dimension(80, 30));
		button[1][1].addActionListener(this);
		button[1][1].setFocusable(false);

		button[1][2] = new JButton();
		button[1][2].setPreferredSize(new Dimension(80, 30));
		button[1][2].addActionListener(this);
		button[1][2].setFocusable(false);

		button[2][0] = new JButton();
		button[2][0].setPreferredSize(new Dimension(80, 30));
		button[2][0].addActionListener(this);
		button[2][0].setFocusable(false);

		button[2][1] = new JButton();
		button[2][1].setPreferredSize(new Dimension(80, 30));
		button[2][1].addActionListener(this);
		button[2][1].setFocusable(false);

		button[2][2] = new JButton();
		button[2][2].setPreferredSize(new Dimension(80, 30));
		button[2][2].addActionListener(this);
		button[2][2].setFocusable(false);

		c.add(button[2][0]);
		c.add(button[2][1]);
		c.add(button[2][2]);
		c.add(button[1][0]);
		c.add(button[1][1]);
		c.add(button[1][2]);
		c.add(button[0][0]);
		c.add(button[0][1]);
		c.add(button[0][2]);

		b.add(user,BorderLayout.WEST);
		b.add(computer,BorderLayout.CENTER);
		b.setBackground(Color.WHITE);

		window.add(c);
		window.add(b,BorderLayout.NORTH);
		window.addKeyListener(this);

		window.setVisible(true);

		//System.out.println("How to play: coord. 1,1 is lower left and 3,3 is top right");
		extra=true;
		/*   while (extra==true){
                    input();
                    changer();
                    System.out.println("\nYour Move:");
                    print();
                    checker();
                    System.out.println("\n-----------------");
                    if (extra==false){
                            continue;
                    }
                    ai();
                    System.out.println("\nComputer's Move:");
                    print();
                    checker();
            }*/
	}

	public void refresh(){
		int i=0;
		int j=0;
		boolean skipAi=false;
		checker();
		if (extra==false){
			if(win==true){
				JOptionPane.showMessageDialog(null, "You Won! Playing Agian...");
				userScore++;
				clearBoard();
				skipAi=true;
			}
			else if(lose==true){
				JOptionPane.showMessageDialog(null, "You Lost... Playing Again...");
				compScore++;
				clearBoard();
				skipAi=true;
			}
			else if(tie==true){
				JOptionPane.showMessageDialog(null, "Tie! Playing Again...");
				clearBoard();
				skipAi=true;
			}
		}
		if(skipAi==false){
			ai();
		}
		else{
			skipAi=false;
		}
		while(i<3){
			while(j<3){
				if(board[i][j]=='O'){
					button[i][j].setText("O");
				}
				j++;
			}
			j=0;
			i++;
		}
		//print();
		//System.out.println("\n");
		i=0;
		j=0;
		checker();
		if (extra==false){
			if(win==true){
				JOptionPane.showMessageDialog(null, "You Won! Playing Agian...");
				userScore++;
				clearBoard();
			}
			else if(lose==true){
				JOptionPane.showMessageDialog(null, "You Lost... Playing Again...");
				compScore++;
				clearBoard();
			}
			else if(tie==true){
				JOptionPane.showMessageDialog(null, "Tie! Playing Again...");
				clearBoard();
			}
			//print();
			//System.out.println("\n");
		}

	}
	public void paintComponent(Graphics g){
		super.paintComponent(g);
	}

	public void keyPressed(KeyEvent e){
	}

	public void keyReleased(KeyEvent e){
	}

	public void keyTyped(KeyEvent e){
		char key = e.getKeyChar();
		if(key=='1'){
			button[0][0].doClick();
		}
		else if(key=='2'){
			button[0][1].doClick();
		}
		else if(key=='3'){
			button[0][2].doClick();
		}
		else if(key=='4'){
			button[1][0].doClick();
		}
		else if(key=='5'){
			button[1][1].doClick();
		}
		else if(key=='6'){
			button[1][2].doClick();
		}
		else if(key=='7'){
			button[2][0].doClick();
		}
		else if(key=='8'){
			button[2][1].doClick();
		}
		else if(key=='9'){
			button[2][2].doClick();
		}
	}

	public void actionPerformed(ActionEvent e){
		JButton button2 = (JButton)e.getSource();

		if (button2 == button[0][0]){
			if(board[0][0]==0){
				board[0][0]='X';
				button[0][0].setText("X");
				refresh();
			}
		}
		else if (button2 == button[0][1]){
			if(board[0][1]==0){
				board[0][1]='X';
				button[0][1].setText("X");
				refresh();
			}
		}
		else if (button2 == button[0][2]){
			if(board[0][2]==0){
				board[0][2]='X';
				button[0][2].setText("X");
				refresh();
			}
		}
		else if (button2 == button[1][0]){
			if(board[1][0]==0){
				board[1][0]='X';
				button[1][0].setText("X");
				refresh();
			}
		}
		else if (button2 == button[1][1]){
			if(board[1][1]==0){
				board[1][1]='X';
				button[1][1].setText("X");
				refresh();
			}
		}
		else if (button2 == button[1][2]){
			if(board[1][2]==0){
				board[1][2]='X';
				button[1][2].setText("X");
				refresh();
			}
		}
		else if (button2 == button[2][0]){
			if(board[2][0]==0){
				board[2][0]='X';
				button[2][0].setText("X");
				refresh();
			}
		}
		else if (button2 == button[2][1]){
			if(board[2][1]==0){
				board[2][1]='X';
				button[2][1].setText("X");
				refresh();
			}
		}
		else if (button2 == button[2][2]){
			if(board[2][2]==0){
				board[2][2]='X';
				button[2][2].setText("X");
				refresh();
			}
		}
	}

	public void clearBoard(){
		int i=0;
		int j=0;
		while(i<3){
			while(j<3){
				board[i][j]=0;
				button[i][j].setText("");
				j++;
			}
			i++;
			j=0;
		}
		win=false;
		tie=false;
		lose=false;
		extra=true;
		user.setText("User: "+userScore);
		computer.setText("Computer: "+compScore);
	}
}