/*
 * **************************************************************************
 * Copyright © 2011-2014 Jay Kamat
 *
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 *
 * This file is part of JPlanet Simulator.
 *
 * JPlanet Simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPlanet Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JPlanet Simulator.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package com.jay;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import jay.jaysound.JayLayer;
import com.jay.graphics.DisplayPanel;
import com.jay.graphics.Menu;
import com.jay.simulation.Space;


/**
 * A lovely program to emulate lovely planets in lovely space :D
 * @author Jay Kamat
 * @author Andrew Goobatic
 * @author Sketch
 * @version 2.1
 */

public class PlanetSim extends JFrame{

	private static final long serialVersionUID = 191631716099655586L;
	private int gameMode;
	public static final int MENU=0,CREATIVE=1,SURVIVAL=2;
	private Menu men; //so creative...
	public Space panel;
	private JPanel top,Game;
	public DisplayPanel panel2;
	private CardLayout cl;
	JayLayer player;
	public static final int TITLE=0,GAME=1;

	static PlanetSim theGameWindow;

	//change this up there too so it follows in the Docs
	public static final String version="2.0.5";

	private boolean isFullScreen=false;
	public PlanetSim() {
		super("JPlanet Simulator  Version "+version);

		/*
		 * attempt new full-screen stuff
		 */
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] devices = env.getScreenDevices();
		isFullScreen = devices[0].isFullScreenSupported();
		this.setUndecorated(isFullScreen);
		this.setResizable(!isFullScreen);
		/*
		if (isFullScreen) {
			// Full-screen mode
			devices[0].setFullScreenWindow(this);
			validate();
		} else {
			// Windowed mode
			pack();
			setVisible(true);

			//mod to set things to the "good old" fullscreen stuff
			this.setUndecorated(true);
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			setBounds(0,0,screenSize.width, screenSize.height);
		}
		*/

		this.setUndecorated(true);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(0,0,screenSize.width, screenSize.height);

		//if needed, heres some window code

		/*
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setBounds(100,100,800,600);
        this.setResizable(false); //this line isn't needed if you keep the undecorated part below set to true
		 */



		//alternate full-screen code
		//getContentPane().setPreferredSize( Toolkit.getDefaultToolkit().getScreenSize());pack();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		men=new Menu(this);
		Game=new JPanel(new BorderLayout());
		top=new JPanel(cl=new CardLayout());
		panel2=new DisplayPanel(this);
		panel = new Space(this);
		panel2.setBackground(Color.WHITE);

		Game.add(panel,BorderLayout.CENTER);
		Game.add(panel2,BorderLayout.NORTH);

		top.add(men,"Menu");
		top.add(Game,"Game");
		add(top);

		player=new JayLayer("/audio/","/audio/",true);
		player.addPlayList();
		player.addPlayList();

		player.addSong(TITLE, "title1.mp3");
		player.addSong(TITLE, "title2.mp3");
		player.addSong(TITLE, "title3.mp3");
		player.addSong(TITLE, "title4.mp3");
		player.addSong(TITLE, "title5.mp3");

		player.addSong(GAME, "game1.mp3");
		player.addSong(GAME, "game2.mp3");
		player.addSong(GAME, "game3.mp3");
		player.addSong(GAME, "game4.mp3");
		player.addSong(GAME, "game5.mp3");

		setResizable(false);
		this.setVisible(true);

		runMenu();
	}

	public static void main(String[] args) throws Exception {

		/*try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e1){
			System.out.println("Finding System Look and Feel Failed. Reverting to Default Java Look and Feel");
		}*/
		// UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		SwingUtilities.invokeAndWait(new Runnable() {
			@Override
			public void run() {
				theGameWindow = new PlanetSim();
			}
		});
		theGameWindow.startUp();
	}
	public void startUp(){
		panel.run();
	}

	public void runMenu(){
		player.changePlayList(TITLE);
		gameMode=PlanetSim.MENU;
		panel.refreshGM();
		panel2.refreshGM();
		cl.show(top,"Menu");
		this.requestFocusInWindow();
	}

	//GM=GameMode GM1=creative GM2=survival GM3=tictactoe
	public void runGM1(){
		player.changePlayList(GAME);
		gameMode=PlanetSim.CREATIVE;
		panel.refreshGM();
		panel2.refreshGM();
		cl.show(top,"Game");
		//thread2.stopSound();
	}

	public void runGM2(){
		player.changePlayList(GAME);
		gameMode=PlanetSim.SURVIVAL;
		panel.refreshGM();
		panel2.refreshGM();
		cl.show(top,"Game");

		panel.repaint();
		panel.killShip();
	}

	public static void runGM3(){
		new Tictactoe();
	}

	public int getGameMode(){
		return gameMode;
	}
}
